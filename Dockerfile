FROM ubuntu:22.04
WORKDIR /libsass
RUN apt-get update
RUN apt-get install -y make cmake git build-essential
RUN git clone https://github.com/sass/libsass.git
